import math
import csv
from difflib import SequenceMatcher

class Pal:
    def __init__(self, name: str, power: int, check_order: int, valid_child: bool) -> None:
        self.name = name
        ''' Name of the Pal. '''
        self.power = power
        ''' Breeding power of the Pal. '''
        self.check_order = check_order
        self.valid_child = valid_child

    def __str__(self) -> str:
        return f'{self.name}: Power: {self.power} Check: {self.check_order}'
    
    def __eq__(self, other: object) -> bool:
        return  isinstance(other, Pal) and self.check_order == other.check_order

pal_data: list[Pal] = []

def search_pal_by_name(pal_name) -> Pal:
    search_sort = sorted(pal_data, key=lambda x: -SequenceMatcher(None, pal_name, x.name).ratio())
    return search_sort[0]


def get_breeding_result(parent_a: Pal, parent_b: Pal) -> Pal:
    # FIXME: Special cases
    baby_power = math.floor((parent_a.power + parent_b.power + 1) / 2.0)
    filtered_pals = [pal for pal in pal_data if pal.valid_child]
    sorted_pals = sorted(filtered_pals, key=lambda x: (abs(x.power - baby_power), x.check_order))
    return sorted_pals[0]

def calculate_parents(target_child: Pal, filter_parent: Pal | None) -> list[tuple[Pal, Pal]]:
    result: list[tuple[Pal, Pal]] = []
    if filter_parent:
        for pal in pal_data:
            test_child = get_breeding_result(filter_parent, pal)
            if (test_child.check_order == target_child.check_order):
                result.append((filter_parent, pal))
    else:
        checked = []
        for pal_a in pal_data:
            for pal_b in pal_data:
                if pal_b in checked:
                    continue
                test_child = get_breeding_result(pal_a, pal_b)
                if (test_child.check_order == target_child.check_order):
                    result.append((pal_a, pal_b))
            checked.append(pal_a)
    return result

def calculate_children(parent: Pal) -> list[tuple[Pal, Pal]]:
    result: list[tuple[Pal, Pal]] = []
    for pal in pal_data:
        child = get_breeding_result(parent, pal)
        if child:
            result.append((pal, child))
    return result

def main():
    print('Palworld Breeding Calculator')
    print('Loading pal data...')
    with open('pal_breed_data.csv', newline='') as csvfile:
        pal_reader = csv.reader(csvfile)
        for row in pal_reader:
            pal_data.append(Pal(row[0], int(row[1]), int(row[3]), row[2].casefold() == 'Yes'.casefold()))
    print('Pal data loaded!')
    print('\n')

    print('1. Find child Pal resulting from two parents.')
    print('2. Find all possible parent combinations to breed this Pal, optionally with a filtered parent.')
    print('3. Show all possible breeding combinations using this Pal as a parent.')
    print('4. Calculates shortest amount of breeding required to get from parent Pal to desired child Pal.')
    print('0. Exit')
    command = int(input('Enter menu item number: '))

    if command == 1:
        parent_a_str = input('Enter first parent: ')
        parent_a = search_pal_by_name(parent_a_str)
        parent_b_str = input('Enter second parent: ')
        parent_b = search_pal_by_name(parent_b_str)
        child = get_breeding_result(parent_a, parent_b)
        if child:
            print(f'{parent_a.name} + {parent_b.name} = {child.name}')
        else:
            print('Failed to find a child that results from these parents.')
    elif command == 2:
        child_str = input('Enter desired child: ')
        child = search_pal_by_name(child_str)
        try:
            parent_str = input('Enter parent filter, or just press Enter to skip: ')
        except EOFError:
            parent_str = None
        parent = search_pal_by_name(parent_str) if parent_str else None

        combo_list = calculate_parents(child, parent)
        
        if len(combo_list) > 0:
            for parent_a, parent_b in sorted(combo_list, key=lambda x: (x[0].name, x[1].name)):
                print(f'{parent_a.name} + {parent_b.name}')
        else:
            print('No valid pairing found. Your parent filter might be invalid.')
    elif command == 3:
        parent_str = input('Enter parent: ')
        parent = search_pal_by_name(parent_str)
        combo_list = calculate_children(parent)
        for other_parent, child in sorted(combo_list, key=lambda x: (x[1].name, x[0].name)):
            print(f'{parent.name} + {other_parent.name} = {child.name}')

if __name__ == '__main__':
    main()

